from rest_framework import serializers
from django.contrib.auth import get_user_model
User = get_user_model()

#user serialiser
class UserSerializer(serializers.ModelSerializer):
  #validator For Phone Number:
  def check_phone(ph):
    for char in ph:
      if not char.isdigit():
        raise serializers.ValidationError("Enter Numbers only")
    if len(ph) != 10:
      raise serializers.ValidationError("Provide Correct Phone")
    return ph

  #validation applied on phone field  
  phone = serializers.CharField(validators=[check_phone])  
  
  #For new user
  def create(self, validated_data):
    user = User.objects.create(email=validated_data['email'])
    user.set_password(validated_data['password'])
    user.save()
    return user

  class Meta:
    model = User
    fields = ['email','name','phone','date_joined','password']

#employee serialiser
class EmployeeSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ['email','date_joined']
    