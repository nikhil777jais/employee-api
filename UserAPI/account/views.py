from django.db.models.query import QuerySet
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserSerializer, EmployeeSerializer
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from account import serializers
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import DjangoModelPermissions, IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import Group
User = get_user_model()

# Create your views here.
class SignUpAPI(APIView):
  def post(self, request, format = None):
    serializer = UserSerializer(data=request.data) #request.data conatain the data that come from post request
    if serializer.is_valid():
      serializer.save()
      user = User.objects.get(email= serializer.data['email']) 
      employee_group = Group.objects.get(name='employee') #assign every user to employee group/category
      user.groups.add(employee_group)
      refresh = RefreshToken.for_user(user) #generate a token for user just while signup
      return Response({'payload': serializer.data, 'refresh': str(refresh), 'access': str(refresh.access_token), 'meassage':'Data Saved!!',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class UserAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions] #for permissions 
  queryset = User.objects.all()

  def get(self, request, id=None, format=None):
    user = User.objects.get(email=request.data['email'])
    if user.groups.filter(name='employee').exists(): #view for employee
      if not id == None:
        user_obj = get_object_or_404(self.queryset, pk=id)
        serializer = EmployeeSerializer(user_obj)
        return Response({'payload': serializer.data, 'meassage':'Data Retrived!!'}, status = status.HTTP_200_OK)
      user_obj = User.objects.all()
      serializer = EmployeeSerializer(user_obj, many= True)
      print(request.data['email'])
      return Response({'payload': serializer.data, 'meassage':'Data Retrived!!'}, status = status.HTTP_200_OK)
    else:  
      if not id == None:  #view for other users(admin)
        user_obj = get_object_or_404(self.queryset, pk=id)
        serializer = UserSerializer(user_obj)
        return Response({'payload': serializer.data, 'meassage':'Data Retrived!!'}, status = status.HTTP_200_OK)
      user_obj = User.objects.all()
      serializer = UserSerializer(user_obj, many= True)
      print(request.data['email'])
      return Response({'payload': serializer.data, 'meassage':'Data Retrived!!'}, status = status.HTTP_200_OK)

  def post(self, request, format = None):
    serializer = UserSerializer(data=request.data) #request.data conatain the data that come from post request
    if serializer.is_valid():
      serializer.save()
      user = User.objects.get(email= serializer.data['email'])
      employee_group = Group.objects.get(name='employee') 
      user.groups.add(employee_group)
      refresh = RefreshToken.for_user(user) # if admin crate user then token will also generate
      return Response({'payload': serializer.data, 'refresh': str(refresh), 'access': str(refresh.access_token), 'meassage':'Data Saved!!',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

  def put(self, request, id = None, format = None):
    user = User.objects.get(pk=id)
    serializer = UserSerializer(user, data = request.data) #request.data get data from user instance
    if serializer.is_valid():
      serializer.save()
      return Response({'payload': serializer.data, 'meassage':'Data Upadted Completely!!'}, status = status.HTTP_200_OK)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

  def patch(self, request, id = None, format = None):
    user = User.objects.get(pk=id)
    serializer = UserSerializer(user, data = request.data, partial = True) #request.data get data from user ins
    if serializer.is_valid():
      serializer.save()
      return Response({'payload': serializer.data, 'meassage':'Data Upadted Patially!!'}, status = status.HTTP_200_OK)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

  def delete(self, request, id = None, format = None):
    user = User.objects.get(pk=id)
    user.delete() 
    return Response({'meassage':'Data Deleted Completely!!'}, status = status.HTTP_200_OK)    