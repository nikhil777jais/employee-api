from django.contrib import admin
from django.urls import path
from . import views
from rest_framework_simplejwt import views as jwt_views
urlpatterns = [
    path('signup/', views.SignUpAPI.as_view(), name='signup' ),
    path('userapi/', views.UserAPI.as_view(), name='userapi' ),
    path('userapi/<int:id>/', views.UserAPI.as_view(), name='userapi' ),
    path('gettoken/', jwt_views.TokenObtainPairView.as_view(), name='gettoken'),
    path('refresh/', jwt_views.TokenRefreshView.as_view(), name='refresh')
]
